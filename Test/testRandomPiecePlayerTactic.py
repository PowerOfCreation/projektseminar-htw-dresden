#!/usr/bin/env python3

import unittest

from GameLogic.Game import Game
from GameLogic.Board import *
from GameLogic.RandomPiecePlayerTactic import *


class TestRandomPiecePlayerTactic(unittest.TestCase):

    def setUp(self):
        self.game = Game()
        self.board = Board(self.game)

    def tearDown(self):
        self.game = None
        self.board = None

    def test_Constructor(self):
        self.playerTactic = RandomPiecePlayerTactic()

    def test_PerformAction(self):
        self.playerTactic = RandomPiecePlayerTactic()
        self.player = Player(self.board, self.playerTactic)
        self.board.add_player(self.player)

        self.assertRaises(IndexError, self.player.perform_action, 1)
        self.assertNotEqual(self.player.perform_action(6), None)

    def test_OnlyOnePieceCanMove(self):
        self.player = Player(self.board, RandomPiecePlayerTactic())
        self.board.add_player(self.player)

        self.player.pieces[0].relative_position = 0

        self.assertEqual(self.player.perform_action(6), self.player.pieces[0])
