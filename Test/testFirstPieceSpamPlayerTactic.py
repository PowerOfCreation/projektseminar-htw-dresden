#!/usr/bin/env python3

import unittest

from GameLogic.Game import Game
from GameLogic.Board import Board
from GameLogic.Player import Player
from GameLogic.FirstPieceSpamPlayerTactic import FirstPieceSpamPlayerTactic


class TestFirstPieceSpamPlayerTactic(unittest.TestCase):
    def test_Constructor(self):
        self.playerTactic = FirstPieceSpamPlayerTactic()
    
    def test_PerformAction(self):
        self.board = Board(Game())
        self.playerTactic = FirstPieceSpamPlayerTactic()
        self.player = Player(self.board, self.playerTactic)
        self.board.add_player(self.player)

        self.assertRaises(IndexError, self.player.perform_action, 1)
        self.assertEqual(self.player.perform_action(6), self.player.pieces[0])
    
    def test_FirstPieceFarthest(self):
        self.board = Board(Game())
        self.player = Player(self.board, FirstPieceSpamPlayerTactic())
        self.board.add_player(self.player)

        self.player.pieces[0].relative_position = 1
        self.player.pieces[1].relative_position = 0

        self.assertEqual(self.player.perform_action(2), self.player.pieces[0])

    def test_SecondPieceFarthest(self):
        self.board = Board(Game())
        self.player = Player(self.board, FirstPieceSpamPlayerTactic())
        self.board.add_player(self.player)

        self.player.pieces[0].relative_position = 0
        self.player.pieces[1].relative_position = 1

        self.assertEqual(self.player.perform_action(2), self.player.pieces[1])
