#!/usr/bin/env python3

import unittest

from GameLogic.Game import Game
from GameLogic.Board import Board
from GameLogic.FirstPieceSpamPlayerTactic import FirstPieceSpamPlayerTactic
from GameLogic.Player import Player


class TestPlayer(unittest.TestCase):

    def setUp(self):
        self.game = Game()
        self.board = Board(self.game)

    def tearDown(self):
        self.game = None
        self.board = None

    def test_PlayerConstructor(self):
        player = Player(self.board, FirstPieceSpamPlayerTactic())
        self.assertTrue(player)
        self.assertEqual(len(player.pieces), 4)

        player = Player(self.board, FirstPieceSpamPlayerTactic(), number_of_pieces=2)
        self.assertTrue(player)
        self.assertEqual(len(player.pieces), 2)

        for piece in player.pieces:
            self.assertEqual(piece.player, player)
            self.assertEqual(piece.relative_position, -1)

    def test_HasPlayerWon(self):
        self.player = self.board.add_player(Player(self.board, FirstPieceSpamPlayerTactic()))

        self.assertFalse(self.player.has_won)

        self.player.pieces[0].relative_position = self.board.total_field_count
        self.player.pieces[1].relative_position = self.board.total_field_count + 1
        self.player.pieces[2].relative_position = self.board.total_field_count + 2
        self.player.pieces[3].relative_position = self.board.total_field_count + 3

        self.assertTrue(self.player.has_won)
