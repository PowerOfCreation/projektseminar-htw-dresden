#!/usr/bin/env python3

from GameLogic.Board import Board
from GameLogic.Game import Game

import unittest


class TestPieceMovement(unittest.TestCase):

    def setUp(self):
        self.game = Game()
        self.board = Board(self.game)

    def tearDown(self):
        self.game = None
        self.board = None

    def test_MovingOut(self):
        self.player = self.board.add_player()
        self.firstPiece = self.player.pieces[0]

        self.assertEqual(self.firstPiece.relative_position, -1)

        self.board.move_by(self.firstPiece, 6)

        self.assertEqual(self.firstPiece.relative_position, 0)

    def test_NotMovingOut(self):
        self.player2 = self.board.add_player()
        self.firstPiece2 = self.player2.pieces[0]

        self.assertEqual(self.firstPiece2.relative_position, -1)

        self.board.move_by(self.firstPiece2, 1)

        self.assertEqual(self.firstPiece2.relative_position, -1)
    
    def test_MovingOutMultiplePlayers(self):
        self.firstPlayer = self.board.add_player()
        self.secondPlayer = self.board.add_player()
        self.firstPiece = self.firstPlayer.pieces[0]
        self.secondPiece = self.secondPlayer.pieces[0]

        self.assertEqual(self.firstPiece.relative_position, -1)
        self.assertEqual(self.secondPiece.relative_position, -1)

        self.board.move_by(self.firstPiece, 6)

        self.assertEqual(self.firstPiece.relative_position, 0)
        self.assertEqual(self.secondPiece.relative_position, -1)

        self.board.move_by(self.secondPiece, 6)

        self.assertEqual(self.firstPiece.relative_position, 0)
        self.assertEqual(self.secondPiece.relative_position, 0)
    
    def test_NormalMove(self):
        self.player = self.board.add_player()
        self.firstPiece = self.player.pieces[0]

        self.board.move_by(self.firstPiece, 6)
        self.board.move_by(self.firstPiece, 1)

        self.assertEqual(self.firstPiece.relative_position, 1)

        self.board.move_by(self.firstPiece, 1)

        self.assertEqual(self.firstPiece.relative_position, 2)

        self.board.move_by(self.firstPiece, 2)

        self.assertEqual(self.firstPiece.relative_position, 4)
    
    def test_FinishField(self):
        self.player = self.board.add_player()
        self.firstPiece = self.player.pieces[0]

        self.board.move_by(self.firstPiece, 6)

        self.board.move_by(self.firstPiece, 6)
        self.board.move_by(self.firstPiece, 4)

        self.assertEqual(self.firstPiece.relative_position, 10)

        self.board.move_by(self.firstPiece, 1)

        self.assertEqual(self.firstPiece.relative_position, 11)

        self.board.move_by(self.firstPiece, 1)

        self.assertEqual(self.firstPiece.relative_position, 12)

        self.board.move_by(self.firstPiece, 1)

        self.assertEqual(self.firstPiece.relative_position, 13)

        self.board.move_by(self.firstPiece, 1)

        self.assertEqual(self.firstPiece.relative_position, 13)

    def test_FinishFieldTwoPieces(self):
        self.player = self.board.add_player()
        self.firstPiece = self.player.pieces[0]
        self.secondPiece = self.player.pieces[1]

        self.board.move_by(self.firstPiece, 6)

        self.board.move_by(self.firstPiece, 6)
        self.board.move_by(self.firstPiece, 4)

        self.assertEqual(self.firstPiece.relative_position, 10)

        self.board.move_by(self.secondPiece, 6)

        self.board.move_by(self.secondPiece, 6)

        self.assertEqual(self.firstPiece.relative_position, 10)
        self.assertEqual(self.secondPiece.relative_position, 6)

        self.board.move_by(self.secondPiece, 4)

        self.assertEqual(self.firstPiece.relative_position, 10)
        self.assertEqual(self.secondPiece.relative_position, 6)

        self.board.move_by(self.secondPiece, 5)

        self.assertEqual(self.firstPiece.relative_position, 10)
        self.assertEqual(self.secondPiece.relative_position, 11)

        self.board.move_by(self.firstPiece, 1)

        self.assertEqual(self.firstPiece.relative_position, 10)
        self.assertEqual(self.secondPiece.relative_position, 11)

        self.board.move_by(self.firstPiece, 2)

        self.assertEqual(self.firstPiece.relative_position, 12)
        self.assertEqual(self.secondPiece.relative_position, 11)

    def test_FinishFieldTwoPlayers(self):
        self.playerOne = self.board.add_player()
        self.firstPiecePlayerOne = self.playerOne.pieces[0]
        self.secondPiecePlayerOne = self.playerOne.pieces[1]

        self.playerTwo = self.board.add_player()

        self.firstPiecePlayerTwo = self.playerTwo.pieces[0]
        self.secondPiecePlayerTwo = self.playerTwo.pieces[1]

        self.board.move_by(self.firstPiecePlayerOne, 6)

        self.board.move_by(self.firstPiecePlayerOne, 6)
        self.board.move_by(self.firstPiecePlayerOne, 4)
        self.board.move_by(self.firstPiecePlayerOne, 6)
        self.board.move_by(self.firstPiecePlayerOne, 4)

        self.assertEqual(self.firstPiecePlayerOne.relative_position, 20)
        self.assertEqual(self.secondPiecePlayerOne.relative_position, -1)
        self.assertEqual(self.firstPiecePlayerTwo.relative_position, -1)
        self.assertEqual(self.secondPiecePlayerTwo.relative_position, -1)

        self.board.move_by(self.firstPiecePlayerTwo, 6)

        self.assertEqual(self.firstPiecePlayerOne.relative_position, 20)
        self.assertEqual(self.secondPiecePlayerOne.relative_position, -1)
        self.assertEqual(self.firstPiecePlayerTwo.relative_position, 0)
        self.assertEqual(self.secondPiecePlayerTwo.relative_position, -1)

        self.board.move_by(self.firstPiecePlayerTwo, 6)
        self.board.move_by(self.firstPiecePlayerTwo, 4)

        self.assertEqual(self.firstPiecePlayerOne.relative_position, 20)
        self.assertEqual(self.secondPiecePlayerOne.relative_position, -1)
        self.assertEqual(self.firstPiecePlayerTwo.relative_position, 10)
        self.assertEqual(self.secondPiecePlayerTwo.relative_position, -1)

        self.board.move_by(self.firstPiecePlayerTwo, 6)
        
        self.assertEqual(self.firstPiecePlayerOne.relative_position, 20)
        self.assertEqual(self.secondPiecePlayerOne.relative_position, -1)
        self.assertEqual(self.firstPiecePlayerTwo.relative_position, 16)
        self.assertEqual(self.secondPiecePlayerTwo.relative_position, -1)

        self.board.move_by(self.firstPiecePlayerTwo, 4)

        self.assertEqual(self.firstPiecePlayerOne.relative_position, 20)
        self.assertEqual(self.secondPiecePlayerOne.relative_position, -1)
        self.assertEqual(self.firstPiecePlayerTwo.relative_position, 20)
        self.assertEqual(self.secondPiecePlayerTwo.relative_position, -1)

        self.board.move_by(self.firstPiecePlayerOne, 4)
        self.board.move_by(self.firstPiecePlayerTwo, 4)

        self.assertEqual(self.firstPiecePlayerOne.relative_position, 20)
        self.assertEqual(self.secondPiecePlayerOne.relative_position, -1)
        self.assertEqual(self.firstPiecePlayerTwo.relative_position, 20)
        self.assertEqual(self.secondPiecePlayerTwo.relative_position, -1)

        self.board.move_by(self.secondPiecePlayerOne, 6)
        self.board.move_by(self.secondPiecePlayerTwo, 6)

        self.assertEqual(self.firstPiecePlayerOne.relative_position, 20)
        self.assertEqual(self.secondPiecePlayerOne.relative_position, 0)
        self.assertEqual(self.firstPiecePlayerTwo.relative_position, 20)
        self.assertEqual(self.secondPiecePlayerTwo.relative_position, 0)

        self.board.move_by(self.firstPiecePlayerOne, 3)
        self.board.move_by(self.firstPiecePlayerTwo, 3)

        self.assertEqual(self.firstPiecePlayerOne.relative_position, 23)
        self.assertEqual(self.secondPiecePlayerOne.relative_position, 0)
        self.assertEqual(self.firstPiecePlayerTwo.relative_position, 23)
        self.assertEqual(self.secondPiecePlayerTwo.relative_position, 0)

        self.board.move_by(self.firstPiecePlayerOne, 1)
        self.board.move_by(self.secondPiecePlayerOne, 1)
        self.board.move_by(self.firstPiecePlayerTwo, 1)
        self.board.move_by(self.secondPiecePlayerTwo, 1)

        self.assertEqual(self.firstPiecePlayerOne.relative_position, 23)
        self.assertEqual(self.secondPiecePlayerOne.relative_position, 1)
        self.assertEqual(self.firstPiecePlayerTwo.relative_position, 23)
        self.assertEqual(self.secondPiecePlayerTwo.relative_position, 1)

    def test_GetPiecePosition(self):
        self.assertEqual(self.board.get_piece_at_position(-1), None)
        self.assertEqual(self.board.get_piece_at_position(0), None)

        self.board.add_player()

        self.firstPiece = self.board.players[0].pieces[0]

        self.assertEqual(self.board.get_piece_at_position(0), None)
        self.assertEqual(self.board.get_piece_at_position(9), None)

        self.board.move_by(self.firstPiece, 9)

        self.assertEqual(self.board.get_piece_at_position(9), None)

        self.board.move_by(self.firstPiece, 6)
        self.board.move_by(self.firstPiece, 9)

        self.assertNotEqual(self.board.get_piece_at_position(9), None)

    def test_KickPiece(self):
        self.firstPlayer = self.board.add_player()
        self.secondPlayer = self.board.add_player()
        self.firstPiece = self.firstPlayer.pieces[0]
        self.secondPiece = self.secondPlayer.pieces[0]

        self.assertEqual(self.firstPiece.relative_position, -1)
        self.assertEqual(self.secondPiece.relative_position, -1)

        self.board.move_by(self.firstPiece, 6)

        self.assertEqual(self.firstPiece.relative_position, 0)
        self.assertEqual(self.secondPiece.relative_position, -1)

        self.board.move_by(self.secondPiece, 6)

        self.assertEqual(self.firstPiece.relative_position, 0)
        self.assertEqual(self.secondPiece.relative_position, 0)
        self.assertEqual(self.firstPiece.absolute_position, 0)
        self.assertEqual(self.secondPiece.absolute_position, self.board.fields_per_player)

        self.board.move_by(self.firstPiece, 6)

        self.assertEqual(self.firstPiece.relative_position, 6)
        self.assertEqual(self.secondPiece.relative_position, 0)
        self.assertEqual(self.firstPiece.absolute_position, 6)
        self.assertEqual(self.secondPiece.absolute_position, self.board.fields_per_player)

        self.board.move_by(self.firstPiece, 4)

        self.assertEqual(self.firstPiece.relative_position, 10)
        self.assertEqual(self.secondPiece.relative_position, -1)
    
    def test_KickPieceBlocked(self):
        self.firstPlayer = self.board.add_player()

        self.firstPiece = self.firstPlayer.pieces[0]
        self.secondPiece = self.firstPlayer.pieces[1]

        self.board.move_by(self.firstPiece, 6)
        self.board.move_by(self.secondPiece, 6)

        self.assertEqual(self.firstPiece.relative_position, 0)
        self.assertEqual(self.secondPiece.relative_position, -1)

    def test_CanKickNotTrue(self):
        self.firstPlayer = self.board.add_player()
        self.firstPiece = self.firstPlayer.pieces[0]

        self.assertFalse(self.firstPiece.can_kick_piece(0))
        self.assertFalse(self.firstPiece.can_kick_piece(1))
        self.assertFalse(self.firstPiece.can_kick_piece(6))

        self.secondPlayer = self.board.add_player()
        self.secondPiece = self.secondPlayer.pieces[0]

        self.assertFalse(self.firstPiece.can_kick_piece(0))
        self.assertFalse(self.firstPiece.can_kick_piece(6))
        self.assertFalse(self.firstPiece.can_kick_piece(1))
        self.assertFalse(self.secondPiece.can_kick_piece(6))
        self.assertFalse(self.secondPiece.can_kick_piece(0))
        self.assertFalse(self.secondPiece.can_kick_piece(1))

        self.firstPiece.relative_position = 0

        self.assertFalse(self.firstPiece.can_kick_piece(0))
        self.assertFalse(self.firstPiece.can_kick_piece(6))
        self.assertFalse(self.firstPiece.can_kick_piece(1))
        self.assertFalse(self.secondPiece.can_kick_piece(6))
        self.assertFalse(self.secondPiece.can_kick_piece(0))
        self.assertFalse(self.secondPiece.can_kick_piece(1))

    def test_CanKickTrue(self):
        self.firstPlayer = self.board.add_player()
        self.secondPlayer = self.board.add_player()
        self.firstPiece = self.firstPlayer.pieces[0]
        self.secondPiece = self.secondPlayer.pieces[0]

        self.assertFalse(self.secondPiece.can_kick_piece(6))

        self.firstPiece.relative_position = self.board.fields_per_player

        self.assertTrue(self.secondPiece.can_kick_piece(6))

        self.firstPiece.relative_position += 1

        self.assertFalse(self.secondPiece.can_kick_piece(6))

        self.secondPiece.relative_position = 0

        self.assertFalse(self.secondPiece.can_kick_piece(6))
        self.assertTrue(self.secondPiece.can_kick_piece(1))
    
    def test_IsInDanger(self):
        self.firstPlayer = self.board.add_player()
        self.secondPlayer = self.board.add_player()
        self.firstPiece = self.firstPlayer.pieces[0]
        self.secondPiece = self.secondPlayer.pieces[0]

        self.assertFalse(self.secondPiece.is_in_danger)

        self.secondPiece.relative_position = 0

        self.assertFalse(self.secondPiece.is_in_danger)

        self.firstPiece.relative_position = 0

        self.assertFalse(self.secondPiece.is_in_danger)

        self.firstPiece.relative_position = self.board.fields_per_player - 6

        self.assertTrue(self.secondPiece.is_in_danger)

        self.firstPiece.relative_position -= 1

        self.assertFalse(self.secondPiece.is_in_danger)

        self.firstPiece.relative_position -= self.board.fields_per_player + 1

        self.assertTrue(self.firstPiece.is_in_danger)
        self.assertFalse(self.secondPiece.is_in_danger)

    def test_GotKickedLastRound(self):
        self.firstPlayer = self.board.add_player()
        self.secondPlayer = self.board.add_player()
        self.firstPiece = self.firstPlayer.pieces[0]
        self.secondPiece = self.secondPlayer.pieces[0]

        self.firstPiece.relative_position = 0
        self.secondPiece.relative_position = 0

        self.assertEqual(self.firstPiece.relative_position, 0)
        self.assertEqual(self.secondPiece.relative_position, 0)
        self.assertFalse(self.firstPiece.got_kicked_last_round)
        self.assertFalse(self.secondPiece.got_kicked_last_round)

        self.board.move_piece_to(self.firstPiece, self.board.fields_per_player)

        self.assertEqual(self.firstPiece.relative_position, self.board.fields_per_player)
        self.assertEqual(self.secondPiece.relative_position, -1)
        self.assertFalse(self.firstPiece.got_kicked_last_round)
        self.assertTrue(self.secondPiece.got_kicked_last_round)





