#!/usr/bin/env python3

import unittest

from GameLogic.FirstPieceSpamPlayerTactic import *
from GameLogic.Game import *


class TestGame(unittest.TestCase):
    def test_Constructor(self):
        self.game = Game()

    def test_RollDice(self):
        self.game = Game()
        self.rolledNumber = self.game.roll_dice()
        self.assertTrue(self.rolledNumber > 0)
        self.assertTrue(self.rolledNumber < 7)

    def test_TurnNoParameter(self):
        self.game = Game()
        self.board = Board(self.game)
        self.player = Player(self.board, FirstPieceSpamPlayerTactic())
        self.board.add_player(self.player)
        self.game.turn()

    def test_OneTurnNoMovingOut(self):
        self.game = Game()
        self.board = Board(self.game)
        self.player = Player(self.board, FirstPieceSpamPlayerTactic())
        self.board.add_player(self.player)

        self.game.turn(1)

        self.assertLessEqual(sum(piece.relative_position for piece in self.player.pieces), -3)

    def test_OneTurnWithMovingOut(self):
        self.game = Game()
        self.board = Board(self.game)
        self.player = Player(self.board, FirstPieceSpamPlayerTactic())
        self.board.add_player(self.player)

        self.game.turn(6)

        self.assertEqual(self.player.pieces[0].relative_position, 0)
        self.assertEqual(self.player.pieces[1].relative_position, -1)
        self.assertEqual(self.player.pieces[2].relative_position, -1)
        self.assertEqual(self.player.pieces[3].relative_position, -1)

    def test_MultipleTurns(self):
        self.game = Game()
        self.board = Board(self.game)
        self.playerTactic = FirstPieceSpamPlayerTactic()
        self.player = Player(self.board, self.playerTactic)
        self.board.add_player(self.player)

        self.game.turn(6)

        self.game.turn(6)
        self.game.turn(1)

        self.assertEqual(self.player.pieces[0].relative_position, 7)
        self.assertEqual(self.player.pieces[1].relative_position, -1)
        self.assertEqual(self.player.pieces[2].relative_position, -1)
        self.assertEqual(self.player.pieces[3].relative_position, -1)
