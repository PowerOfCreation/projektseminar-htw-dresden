#!/usr/bin/env python3

import unittest

from GameLogic.Game import Game
from GameLogic.Board import Board
from GameLogic.Player import Player
from GameLogic.LastPieceSpamForceKickPlayerTactic import LastPieceSpamForceKickPlayerTactic


class TestLastPieceSpamForceKickPlayerTactic(unittest.TestCase):
    def test_Constructor(self):
        self.playerTactic = LastPieceSpamForceKickPlayerTactic()
    
    def test_PerformAction(self):
        self.board = Board(Game())
        self.playerTactic = LastPieceSpamForceKickPlayerTactic()
        self.player = Player(self.board, self.playerTactic)
        self.board.add_player(self.player)

        self.assertRaises(IndexError, self.player.perform_action, 1)
        self.assertEqual(self.player.perform_action(6), self.player.pieces[0])
    
    def test_FirstPieceFarthest(self):
        self.board = Board(Game())
        self.player = Player(self.board, LastPieceSpamForceKickPlayerTactic())
        self.board.add_player(self.player)

        self.player.pieces[0].relative_position = 1
        self.player.pieces[1].relative_position = 0

        self.assertEqual(self.player.perform_action(2), self.player.pieces[1])

    def test_SecondPieceFarthest(self):
        self.board = Board(Game())
        self.player = Player(self.board, LastPieceSpamForceKickPlayerTactic())
        self.board.add_player(self.player)

        self.player.pieces[0].relative_position = 0
        self.player.pieces[1].relative_position = 1

        self.assertEqual(self.player.perform_action(2), self.player.pieces[0])

    def test_FirstPieceCanKick(self):
        self.board = Board(Game())
        self.firstPlayer = Player(self.board, LastPieceSpamForceKickPlayerTactic())
        self.secondPlayer = Player(self.board, LastPieceSpamForceKickPlayerTactic())
        self.board.add_player(self.firstPlayer)
        self.board.add_player(self.secondPlayer)

        self.firstPlayer.pieces[0].relative_position = self.board.fields_per_player + 4

        self.secondPlayer.pieces[0].relative_position = 0
        self.secondPlayer.pieces[1].relative_position = 1

        self.assertEqual(self.secondPlayer.perform_action(2), self.secondPlayer.pieces[0])
        self.assertEqual(self.secondPlayer.perform_action(3), self.secondPlayer.pieces[1])
        self.assertEqual(self.secondPlayer.perform_action(4), self.secondPlayer.pieces[0])
