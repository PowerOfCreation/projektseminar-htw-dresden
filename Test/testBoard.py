#!/usr/bin/env python3

import unittest

from GameLogic.Game import Game
from GameLogic.Board import Board
from GameLogic.FirstPieceSpamPlayerTactic import FirstPieceSpamPlayerTactic
from GameLogic.Player import Player


class TestBoard(unittest.TestCase):

    def test_BoardConstructor(self):
        self.assertTrue(Board(Game()))

    def test_AddPlayerNoParameter(self):
        board = Board(Game())

        self.assertEqual(len(board.players), 0)

        added_player = board.add_player()

        self.assertEqual(len(board.players), 1)
        self.assertTrue(added_player)

    def test_AddPlayerWithParameter(self):
        board = Board(Game())
        player = Player(board, FirstPieceSpamPlayerTactic())

        self.assertEqual(len(board.players), 0)

        added_player = board.add_player(player)

        self.assertEqual(len(board.players), 1)
        self.assertEqual(player, added_player)
        self.assertTrue(added_player)
