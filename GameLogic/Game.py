#!/usr/bin/env python3

from typing import Optional

import random

from GameLogic.Board import Board
from GameLogic.Player import Player
from GameLogic.Piece import Piece


class Game:
    """
    The ludo Game. Can be reused by setting each players pieces relative_positions to -1.

    :ivar rolled_number: The last number the dice has rolled. Used because the PlayerTactic class is deciding which turn it takes and then calls execute_turn with the chosen piece.
    :ivar board: The board of this game.
    :ivar currentTurn: The index of the player which turns it is. For four players this value is always between 0 and 3.
    """

    def __init__(self) -> None:
        self.rolled_number = 0
        self.board = None
        self.currentTurn = 0

    def set_board(self, board: Board):
        self.board = board

    def run_game_loop(self) -> Player:
        while self.get_winner() is None:
            self.turn()

        return self.get_winner()

    def get_winner(self) -> Optional[Player]:
        return next((player for player in self.board.players if player.has_won), None)

    def turn(self, rolled_number: int = 0) -> None:
        if rolled_number == 0:
            self.rolled_number = self.roll_dice()
        else:
            self.rolled_number = rolled_number

        current_player: Player = self.board.players[self.currentTurn]

        attempt = 0

        while attempt < 3 and not any(piece.can_move(self.rolled_number) for piece in current_player.pieces):
            attempt += 1
            self.rolled_number = self.roll_dice()

        if attempt < 3 and any(piece.can_move(self.rolled_number) for piece in current_player.pieces):
            current_player.perform_action(self.rolled_number)

        for piece in current_player.pieces:
            piece.got_kicked_last_round = False

        self.currentTurn = (self.currentTurn + 1) % len(self.board.players)

    def execute_turn(self, piece: Piece) -> float:
        if piece is None or not piece.can_move(self.rolled_number):
            return -10

        self.board.move_by(piece, self.rolled_number)

        reward = 1.0 / ((self.board.total_field_count + 4) * 4)

        reward += 1 if piece.player.has_won else 0

        return reward

    @staticmethod
    def roll_dice() -> int:
        return random.randint(1, 6)
    
    