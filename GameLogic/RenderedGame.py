#!/usr/bin/env python3

from typing import Optional

from GameLogic.Board import Board
from GameLogic.Player import Player
from GameLogic.Piece import Piece
from GameLogic.Game import Game

import pygame
import time

class RenderedGame(Game):
    """
    Will render the game on a window using Pygame.
    """

    def __init__(self, wait_between_dice_rolls = 300) -> None:
        super(RenderedGame, self).__init__()
        pygame.init()

        self.screen = pygame.display.set_mode([800, 800])
        self.running = True
        self.fields_distance_from_center = 300
        self.wait_between_dice_rolls = wait_between_dice_rolls


    def run_game_loop(self) -> Optional[Player]:
        self.board.render(self)
        pygame.display.flip()

        while self.get_winner() is None and self.running:
            # Did the user click the window close button?
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
            self.turn()

        return self.get_winner()

    def turn(self, rolled_number: int = 0) -> None:
        if rolled_number == 0:
            pygame.time.wait(self.wait_between_dice_rolls)
            self.rolled_number = self.roll_dice()
        else:
            self.rolled_number = rolled_number

        current_player: Player = self.board.players[self.currentTurn]

        attempt = 0

        while attempt < 3 and not any(piece.can_move(self.rolled_number) for piece in current_player.pieces):
            attempt += 1
            pygame.time.wait(self.wait_between_dice_rolls)
            self.rolled_number = self.roll_dice()

        if attempt < 3 and any(piece.can_move(self.rolled_number) for piece in current_player.pieces):
            current_player.perform_action(self.rolled_number)

        self.currentTurn = (self.currentTurn + 1) % len(self.board.players)

        self.board.render(self)

        for player in self.board.players:
            player.render(self)
        
        pygame.display.flip()

    @property
    def screen_center(self):
        return pygame.Vector2(self.screen.get_width() / 2, self.screen.get_height() / 2)

    @staticmethod
    def roll_dice() -> int:
        return_value = Game.roll_dice()
        print(f"Rolled the dice and got {str(return_value)}")
        return return_value
