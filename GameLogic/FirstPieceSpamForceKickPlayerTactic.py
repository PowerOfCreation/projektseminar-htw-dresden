#!/usr/bin/env python3

from GameLogic.Piece import Piece
from GameLogic.Player import PlayerTactic


class FirstPieceSpamForceKickPlayerTactic(PlayerTactic):
    def perform_action(self, pieces: list[Piece], rolled_number: int) -> Piece:
        pieces = list(piece for piece in pieces if piece.can_move(rolled_number))

        pieces_which_can_kick = list(piece for piece in pieces if piece.can_kick_piece(rolled_number))

        pieces = pieces_which_can_kick if len(pieces_which_can_kick) > 0 else pieces

        pieces.sort(key=lambda x: x.relative_position, reverse=True)

        self.game.execute_turn(pieces[0])
        return pieces[0]
