#!/usr/bin/env python3

from __future__ import annotations
from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

import numpy as np
import random

if TYPE_CHECKING:
    from GameLogic.Board import Board
    from GameLogic.Game import Game
    from GameLogic.Piece import Piece


# abstract class
class PlayerTactic(ABC):
    """
    The brain of the player. Decides which piece to move. perform_action gets called form the Game class.
    After deciding which piece to move it will call the method Game.execute_turn(chosen_piece).
    """

    def __init__(self):
        self.game = None
        self.player = None

    def set_game(self, game: Game) -> None:
        self.game = game

    def set_player(self, player: Player) -> None:
        self.player = player

    @abstractmethod
    def perform_action(self, pieces: list[Piece], rolled_number: int) -> Piece:
        pass


class Player:
    """
    One player has four pieces.

    :ivar board: The game board this player is participating on.
    :ivar player_tactic: The brain of this player. The player tactic is deciding, which piece to move.
    :ivar pieces: The four pieces this player owns.
    :ivar color: The color of this player. Used for rendering in combination with the RenderedGame class only.
    """

    def __init__(self, board: Board, player_tactic: PlayerTactic, number_of_pieces: int = 4, color = None ) -> None:
        from GameLogic.Piece import Piece
        self.board = board
        self.playerTactic = player_tactic
        self.playerTactic.set_player(self)
        self.playerTactic.set_game(self.board.game)
        self.pieces = [Piece(self) for _ in range(number_of_pieces)]
        if color is not None:
            self.color = color
        else:
            self.color = (random.random() * 255, random.random() * 255, random.random() * 255)

    def perform_action(self, rolled_number: int) -> Piece:
        return self.playerTactic.perform_action(self.pieces, rolled_number)

    @property
    def start_position(self) -> int:
        return self.board.get_player_position(self) * self.board.fields_per_player

    @property
    def has_won(self) -> bool:
        return all(piece.is_in_finish_fields for piece in self.pieces)

    def get_player_state(self):
        rolled_number = self.board.game.rolled_number

        player_state = []

        player_state.append(rolled_number / 6)
        player_state.extend([piece.relative_position / (self.board.total_field_count + 3) for piece in self.pieces])
        player_state.extend([self.board.get_piece_at_position(index) is None for index in range(0, self.board.total_field_count)])

        return np.array(player_state)
    
    def render(self, rendered_game):
        for piece in self.pieces:
            piece.render(rendered_game)

    def reset_pieces(self):
        for piece in self.pieces:
            piece.relative_position = -1
            piece.got_kicked_last_round = False
