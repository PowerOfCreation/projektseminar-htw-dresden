from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from GameLogic.Player import Player


class Piece:
    """
    A piece owned by one player. Each player has four pieces.

    :ivar player: Player which owns this piece.
    :ivar relative_position: Position relative from the player start field. -1 means it is not on the field yet. A value >= than the total field size means it is in the finish fields.
    :ivar got_kicked_last_round: True when this piece has been kicked. Resets when the owning players turn is made the next time. Used for calculating reward for lost pieces.
    """

    def __init__(self, player: Player, relative_position: int = -1) -> None:
        self.player = player
        self.relative_position = relative_position
        self.got_kicked_last_round = False

    @property
    def absolute_position(self) -> int:
        return self.relative_position_to_absolute_position(self.relative_position)

    def relative_position_to_absolute_position(self, relative_position: int) -> int:
        if relative_position == -1:
            return -1

        # Piece is already in the finish fields and not on the actual board anymore
        if relative_position >= self.player.board.total_field_count:
            return -1

        return (self.player.start_position + relative_position) % self.player.board.total_field_count

    def get_relative_target_position(self, rolled_number: int) -> int:
        target_position: int = 0

        if self.relative_position == -1:
            if rolled_number != 6:
                return -1
        else:
            target_position = self.relative_position + rolled_number

            if target_position >= self.player.board.total_field_count + 4:
                return -1

        return target_position

    def can_move(self, rolled_number: int) -> bool:
        if self.relative_position == -1 and rolled_number != 6:
            return False

        relative_target_position: int = self.get_relative_target_position(rolled_number)

        if relative_target_position == -1:
            return False

        return sum(1 for piece in self.player.pieces if piece.relative_position == relative_target_position) == 0

    def can_kick_piece(self, rolled_number: int) -> bool:
        relative_target_position: int = self.get_relative_target_position(rolled_number)
        absolute_target_position: int = self.relative_position_to_absolute_position(relative_target_position)

        if relative_target_position == -1:
            return False

        other_piece = self.player.board.get_piece_at_position(absolute_target_position)

        if other_piece is not None and other_piece.player != self.player:
            return True

        return False

    def kick(self):
        self.player.board.move_piece_to(self, -1)
        self.got_kicked_last_round = True

    @property
    def is_in_danger(self) -> bool:
        if self.is_in_finish_fields or self.relative_position == -1:
            return False

        player: Player
        for player in self.player.board.players:

            if player != self.player:

                piece: Piece
                for piece in player.pieces:
                    other_piece_absolute_position = piece.absolute_position
                    if other_piece_absolute_position > -1:
                        if other_piece_absolute_position < self.absolute_position and other_piece_absolute_position + 6 >= self.absolute_position:
                            return True
        
        return False

    @property
    def is_in_finish_fields(self) -> bool:
        return self.relative_position >= self.player.board.total_field_count

    def render(self, rendered_game):
        import pygame

        if self.relative_position == -1:
            position_on_screen = self.player.board.get_start_field_position_in_render(rendered_game, self)
        elif self.is_in_finish_fields:
            position_on_screen = self.player.board.get_finish_field_position_in_render(self.player.board.players.index(self.player), self.relative_position - self.player.board.total_field_count)
        else:
            position_on_screen = self.player.board.get_field_position_in_render(rendered_game, self.absolute_position)

        pygame.draw.circle(rendered_game.screen, self.player.color, position_on_screen, 15)
