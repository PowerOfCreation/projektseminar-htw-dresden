#!/usr/bin/env python3

from __future__ import annotations

from typing import Optional
from typing import Container
from typing import TYPE_CHECKING

from GameLogic.Piece import Piece
from GameLogic.Player import Player

import math

if TYPE_CHECKING:
    from GameLogic.FirstPieceSpamPlayerTactic import FirstPieceSpamPlayerTactic
    from GameLogic.Game import Game


class Board:

    fields_per_player = 10

    def __init__(self, game: Game) -> None:
        self.players: Container[Player] = []
        self.game: Game = game
        self.game.set_board(self)

    def add_player(self, player: Player = None) -> Player:
        if player is None:
            from GameLogic.FirstPieceSpamPlayerTactic import FirstPieceSpamPlayerTactic
            player = Player(self, FirstPieceSpamPlayerTactic())
        
        self.players.append(player)
        return player

    @property
    def total_field_count(self) -> int:
        return len(self.players) * self.fields_per_player

    def get_player_position(self, player: Player) -> int:
        return self.players.index(player)

    def get_relative_piece_position(self, player: Player, absolute_position: int) -> int:
        if absolute_position >= player.start_position:
            return absolute_position - player.start_position
        else:
            return absolute_position + self.total_field_count - player.start_position

    def get_piece_at_position(self, position: int) -> Optional[Piece]:
        if position < 0 or position >= self.total_field_count:
            return None

        for player in self.players:
            for piece in player.pieces:
                if piece.absolute_position == position:
                    return piece
                
    def move_by(self, piece: Piece, advance_by: int) -> bool:
        if piece.relative_position == -1:
            if advance_by == 6:
                return self.move_piece_to(piece, piece.player.start_position)
        elif piece.relative_position + advance_by < self.total_field_count:
            return self.move_piece_to(piece, piece.absolute_position + advance_by)
        else:
            return self.move_piece_to_finish_position(piece, piece.relative_position + advance_by)

    def move_piece_to(self, piece: Piece, absolute_position: int) -> bool:
        if absolute_position == -1:
            piece.relative_position = -1
            return True

        other_piece = self.get_piece_at_position(absolute_position)

        if other_piece is None:
            piece.relative_position = self.get_relative_piece_position(piece.player, absolute_position)
            return True
        
        if piece.player != other_piece.player:
            piece.relative_position = self.get_relative_piece_position(piece.player, absolute_position)
            other_piece.kick()
            return True

        return False
    
    def move_piece_to_finish_position(self, piece: Piece, target_relative_position: int) -> bool:
        if target_relative_position < self.total_field_count or target_relative_position >= self.total_field_count + 4:
            return False
        
        for playerPiece in piece.player.pieces:
            if playerPiece.relative_position == target_relative_position:
                return False
        
        piece.relative_position = target_relative_position

        return True

    @property
    def radians_per_player(self) -> float:
        return (2 * math.pi) / len(self.players)

    def get_player_index_who_owns_field(self, absolute_field_position: int) -> Player:
        return int(absolute_field_position / self.fields_per_player)

    def get_player_who_owns_field(self, absolute_field_position: int) -> Player:
        return self.players[self.get_player_index_who_owns_field(absolute_field_position)]

    def get_player_first_field_position_in_render(self, player_index: int):
        import pygame

        current_player_radians = self.radians_per_player * player_index

        return pygame.Vector2(math.cos(current_player_radians) * self.game.fields_distance_from_center, math.sin(current_player_radians) * self.game.fields_distance_from_center)


    def get_field_position_in_render(self, rendered_game, absolute_field_position: int):
        player_index = self.get_player_index_who_owns_field(absolute_field_position)

        current_player_first_field_position = self.get_player_first_field_position_in_render(player_index)
        target_player_first_field_position = self.get_player_first_field_position_in_render(player_index + 1)

        fields_direction = target_player_first_field_position - current_player_first_field_position

        relative_field_index = absolute_field_position % self.fields_per_player

        return self.game.screen_center + (current_player_first_field_position + (fields_direction * (relative_field_index / self.fields_per_player)))

    def get_start_field_position_in_render(self, rendered_game, piece: Piece):
        player_index = rendered_game.board.players.index(piece.player)
        piece_index = piece.player.pieces.index(piece)

        current_player_first_field_position = self.get_player_first_field_position_in_render(player_index)
        target_player_first_field_position = self.get_player_first_field_position_in_render(player_index + 1)

        fields_direction = target_player_first_field_position - current_player_first_field_position
        distance_between_fields = (fields_direction * (1 / self.fields_per_player)).magnitude()

        if piece_index < len(piece.player.pieces) / 2:
            field_position = self.game.screen_center + current_player_first_field_position + current_player_first_field_position.normalize() * distance_between_fields * (piece_index + 1)
        else:
            offset = current_player_first_field_position.normalize().rotate(90) * distance_between_fields
            field_position = offset + self.game.screen_center + current_player_first_field_position + current_player_first_field_position.normalize() * distance_between_fields * (piece_index + 1 - len(piece.player.pieces) / 2) 

        return field_position

    def get_finish_field_position_in_render(self, player_index: int, finish_field_index: int):
        current_player_first_field_position = self.get_player_first_field_position_in_render(player_index)
        target_player_first_field_position = self.get_player_first_field_position_in_render(player_index + 1)

        fields_direction = target_player_first_field_position - current_player_first_field_position
        distance_between_fields = (fields_direction * (1 / self.fields_per_player)).magnitude()

        return self.game.screen_center + current_player_first_field_position - current_player_first_field_position.normalize() * distance_between_fields * (finish_field_index + 1)

    def render(self, rendered_game):
        import pygame

        rendered_game.screen.fill((255, 255, 255))

        for player_index, player in enumerate(self.players):
            current_player_first_field_position = self.get_player_first_field_position_in_render(player_index)
            target_player_first_field_position = self.get_player_first_field_position_in_render(player_index + 1)

            fields_direction = target_player_first_field_position - current_player_first_field_position
            
            screen_center = (rendered_game.screen.get_width() / 2, rendered_game.screen.get_height() / 2)

            # fields between player starts
            for field in range(self.fields_per_player):
                field_position = screen_center + (current_player_first_field_position + (fields_direction * (field / self.fields_per_player)))
                pygame.draw.circle(rendered_game.screen, (0, 0, 255), field_position, 15)

            # start fields
            for piece in player.pieces:
                field_position = self.get_start_field_position_in_render(rendered_game, piece)
                pygame.draw.circle(rendered_game.screen, (0, 0, 255), field_position, 15)

            # finish fields
            for field_index, _ in enumerate(player.pieces):
                field_position = self.get_finish_field_position_in_render(player_index, field_index)
                pygame.draw.circle(rendered_game.screen, (0, 0, 255), field_position, 15)


    def reset(self):
        for player in self.players:
            player.reset_pieces()