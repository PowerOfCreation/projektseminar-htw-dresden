#!/usr/bin/env python3
import random

from GameLogic.Piece import Piece
from GameLogic.Player import PlayerTactic


class RandomPiecePlayerTactic(PlayerTactic):
    def perform_action(self, pieces: list[Piece], rolled_number: int) -> Piece:
        pieces = list(piece for piece in pieces if piece.can_move(rolled_number))

        piece = random.choice(pieces)

        self.game.execute_turn(piece)

        return piece
