#!/usr/bin/env python3

from GameLogic.Piece import Piece
from GameLogic.Player import PlayerTactic


class FirstPieceSpamPlayerTactic(PlayerTactic):
    def perform_action(self, pieces: list[Piece], rolled_number: int) -> Piece:
        pieces = list(piece for piece in pieces if piece.can_move(rolled_number))

        pieces.sort(key=lambda x: x.relative_position, reverse=True)

        self.game.execute_turn(pieces[0])
        return pieces[0]
