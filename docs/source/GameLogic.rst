GameLogic package
=================

Submodules
----------

GameLogic.Board module
----------------------

.. automodule:: GameLogic.Board
   :members:
   :undoc-members:
   :show-inheritance:

GameLogic.FirstPieceSpamPlayerTactic module
-------------------------------------------

.. automodule:: GameLogic.FirstPieceSpamPlayerTactic
   :members:
   :undoc-members:
   :show-inheritance:

GameLogic.Game module
---------------------

.. automodule:: GameLogic.Game
   :members:
   :undoc-members:
   :show-inheritance:

GameLogic.LastPieceSpamForceKickPlayerTactic module
---------------------------------------------------

.. automodule:: GameLogic.LastPieceSpamForceKickPlayerTactic
   :members:
   :undoc-members:
   :show-inheritance:

GameLogic.LastPieceSpamPlayerTactic module
------------------------------------------

.. automodule:: GameLogic.LastPieceSpamPlayerTactic
   :members:
   :undoc-members:
   :show-inheritance:

GameLogic.Piece module
----------------------

.. automodule:: GameLogic.Piece
   :members:
   :undoc-members:
   :show-inheritance:

GameLogic.Player module
-----------------------

.. automodule:: GameLogic.Player
   :members:
   :undoc-members:
   :show-inheritance:

GameLogic.RandomPiecePlayerTactic module
----------------------------------------

.. automodule:: GameLogic.RandomPiecePlayerTactic
   :members:
   :undoc-members:
   :show-inheritance:

GameLogic.RenderedGame module
-----------------------------

.. automodule:: GameLogic.RenderedGame
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: GameLogic
   :members:
   :undoc-members:
   :show-inheritance:
