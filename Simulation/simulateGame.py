#!/usr/bin/env python3
from GameLogic.Board import Board
from GameLogic.Game import Game
from GameLogic.FirstPieceSpamPlayerTactic import FirstPieceSpamPlayerTactic
from GameLogic.LastPieceSpamPlayerTactic import LastPieceSpamPlayerTactic
from GameLogic.FirstPieceSpamForceKickPlayerTactic import FirstPieceSpamForceKickPlayerTactic
from GameLogic.LastPieceSpamForceKickPlayerTactic import LastPieceSpamForceKickPlayerTactic
from GameLogic.Player import Player
from GameLogic.RandomPiecePlayerTactic import RandomPiecePlayerTactic

import matplotlib.pyplot as plt

import string

NUMBER_OF_GAMES=10000

PLAYER_COUNT = 4

count_of_player_wins = [0] * PLAYER_COUNT

for game_index in range(NUMBER_OF_GAMES):
    game = Game()
    board = Board(game)

    for player_index in range(PLAYER_COUNT):
        if player_index < PLAYER_COUNT / 2:
            board.add_player(Player(board, RandomPiecePlayerTactic()))
        else:
            board.add_player(Player(board, FirstPieceSpamForceKickPlayerTactic()))

    game.run_game_loop()

    count_of_player_wins[board.players.index(game.get_winner())] += 1

for wins in count_of_player_wins:
    print(wins)

remove_lowercase_table = str.maketrans('', '', string.ascii_lowercase)

file_name = f"{str(NUMBER_OF_GAMES)}_"

player: Player
for (index, player) in enumerate(board.players):
    plt.bar(f"{player.playerTactic.__class__.__name__.translate(remove_lowercase_table)} {index}", count_of_player_wins[index])
    file_name += f"{player.playerTactic.__class__.__name__}_"

file_name = file_name[:-1]
file_name += ".jpg"

plt.savefig(file_name)