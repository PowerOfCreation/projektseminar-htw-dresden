#!/usr/bin/env python3
from GameLogic.Board import Board
from GameLogic.Game import Game
from GameLogic.RenderedGame import RenderedGame
from GameLogic.LastPieceSpamForceKickPlayerTactic import LastPieceSpamForceKickPlayerTactic
from GameLogic.FirstPieceSpamPlayerTactic import FirstPieceSpamPlayerTactic 
from GameLogic.Player import Player
from ReinforcementLearning.DQNPlayerTactic import DQNPlayerTactic

from tqdm import tqdm

render_window=False
PLAYER_COUNT = 4

count_of_player_wins = [0] * PLAYER_COUNT

for game_index in tqdm(range(50)):
    game = RenderedGame() if render_window else Game()
    board = Board(game)

    for player_index in range(PLAYER_COUNT):
        if player_index < PLAYER_COUNT / 2:
            dqn_player_tactic: DQNPlayerTactic = board.add_player(Player(board, DQNPlayerTactic(is_training=False))).playerTactic
            dqn_player_tactic.generate_models()
            dqn_player_tactic.load_weights("best_model_weights")
        else:
            board.add_player(Player(board, FirstPieceSpamPlayerTactic()))

    game.run_game_loop()

    count_of_player_wins[board.players.index(game.get_winner())] += 1

for wins in count_of_player_wins:
    print(wins)
