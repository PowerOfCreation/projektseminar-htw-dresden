#!/usr/bin/env python3
from typing import Optional

from GameLogic.Board import Board
from GameLogic.Game import Game
from GameLogic.Player import Player
from ReinforcementLearning.DQNPlayerTactic import DQNPlayerTactic

from tqdm import tqdm

import math

episodes = 200

PLAYER_COUNT = 4

count_of_player_wins = [0] * PLAYER_COUNT

game = Game()
board = Board(game)

highest_reward = -math.inf

for player_index in range(PLAYER_COUNT):
    dqn_player_tactic: DQNPlayerTactic = board.add_player(Player(board, DQNPlayerTactic(episode=0))).playerTactic

player: Player
for player in board.players:
    player.playerTactic.generate_models()

for game_index in tqdm(range(episodes)):
    winner: Player = game.run_game_loop()
    winnerPlayerTactic: DQNPlayerTactic = winner.playerTactic

    reward = winnerPlayerTactic.total_training_rewards

    print("Rewards: " + str(reward))

    if reward > highest_reward and game_index > 20:
        highest_reward = reward
        winnerPlayerTactic.save_weights("best_model_weights")

    winnerPlayerTactic.save_weights()

    board.reset()

    for player in board.players:
        # Synchronize weights for both winner and loser
        player.playerTactic.load_weights()
        player.playerTactic.reset()
        player.playerTactic.episode += 1

    for player in board.players:
        print("Epsilon: " + str(player.playerTactic.epsilon))
