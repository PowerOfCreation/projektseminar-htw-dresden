# Projektseminar HTW Dresden - Niklas Werner

## Docs

The documentation for this project can be found under docs/tex/projektseminar.pdf.

## Requirements

It is important to have Python 3.10 or higher, older Python versions will **NOT** work.

- Python 3.10 or higher
- Pip

## Installation 

All required dependencies for executing tests and simulating the game without NN or visualization are in the requirements.txt file.
For NN and visualization you will need to install Tensorflow and Pygame as well.

```bash
pip install -r requirements.txt
pip install tensorflow==2.10.0 pygame==2.1.2
```

## Executing Tests

```bash
coverage run -m unittest discover
```

Optional to see the coverage:

```bash
coverage report
```

## Code Documentation

Documentation can be found when opening `docs/build/html/index.html`.

## Executing Ludo Simulation without Neural Networks

This executes a few hundred ludo games with agents, which have hard coded rules to follow.

Windows Powershell:
```bash
$env:PYTHONPATH="."
python .\Simulation\simulateGame.py
```

Linux:
```bash
export PYTHONPATH="."
python Simulation/simulateGame.py
```

## Training DQN

This trains a Deep-Q-Network by playing against itself. The winner network gets chosen for the next game and the network with the best reward gets saved to the best_model_weights folder.

Windows Powershell:
```bash
$env:PYTHONPATH="."
python .\Simulation\trainDQN.py
```

Linux:
```bash
export PYTHONPATH="."
python Simulation/trainDQN.py
```

## Executing Ludo Simulation with Neural Networks

This is for simulating games. Before this can be executed it is necessary to follow the steps in the previous section in order to train an agent.

Windows Powershell:
```bash
$env:PYTHONPATH="."
python .\Simulation\simulateGameWithDQN.py
```

Linux:
```bash
export PYTHONPATH="."
python Simulation/simulateGameWithDQN.py
```

## Folder Structure

**GameLogic** as the name already suggests contains all logic to simulate a game of Ludo.
It contains some Tactics as well like "FirstPieceSpamPlayerTactic" or "LastPieceSpamPlayerTactic" which are following set rules to play Ludo.
If you want to code your own Tactic then just duplicate "FirstPieceSpamPlayerTactic.py", give it a new name and you can code your own strategy.

**Notebooks** have been used to experiment with various approaches to AI using ReinforcementLearning. They are not required to run Ludo.

**ReinforcementLearning** contains a Tactic just like the files in **GameLogic**, but here a much more sophisticated approach using a DQN has been used. Here a DQN chooses which piece to move.

**Results** is just a list of results to show.

**Simulation** this folder contains code to simulate a game of Ludo or do simulate a game of Ludo while training a DQN.
The simulation can be visualized by using the RenderedGame class instead of the Game class. Simply replace `game = Game()` with `game = RenderedGame()`.

**Test** contains unit tests.

## Updating documentation

First you need to install sphinx https://www.sphinx-doc.org/en/master/usage/installation.html.
Then:

Linux:
```bash
cd docs
sphinx-apidoc -f -o source ../GameLogic
make html
```

Windows:
```bash
cd docs
sphinx-apidoc -f -o source ../GameLogic
./make.bat html
```