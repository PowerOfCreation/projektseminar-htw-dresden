#!/usr/bin/env python3
import random

from typing import Optional

from collections import deque

from GameLogic.Player import PlayerTactic
from GameLogic.Piece import Piece
import tensorflow as tf
import numpy as np

from pathlib import Path

model_path_string = "model"
target_model_path_string = "target_model"

# https://github.com/mswang12/minDQN/blob/main/minDQN.py
class DQNPlayerTactic(PlayerTactic):
    def __init__(self, is_training: bool = True, episode: int = 0, epsilon = 1):
        super().__init__()
        self.is_training = is_training
        self.model = None
        self.target_model = None
        self.replay_memory = deque(maxlen=50_000)
        self.epsilon = epsilon
        self.max_epsilon = epsilon  # You can't explore more than 100% of the time
        self.min_epsilon = 0.01  # At a minimum, we'll always explore 1% of the time
        self.decay = 0.01
        self.steps_taken = 0
        self.total_training_rewards = 0
        self.episode = episode
        self.last_reward: float = 0
        self.last_moved_piece: Optional[Piece] = 0
        self.last_current_state = None
        self.last_new_state = None

    def generate_models(self):
        self.model = self._build_model()
        self.target_model = self._build_model()
        self.target_model.set_weights(self.model.get_weights())

    def _build_model(self):
        learning_rate = 0.001
        model = tf.keras.Sequential()
        model.add(tf.keras.layers.Dense(24, input_shape=self.player.get_player_state().shape, activation='relu', kernel_initializer=tf.keras.initializers.HeUniform()))
        model.add(tf.keras.layers.Dense(12, activation='relu', kernel_initializer=tf.keras.initializers.HeUniform()))
        model.add(tf.keras.layers.Dense(len(self.player.pieces), activation='linear', kernel_initializer=tf.keras.initializers.HeUniform()))
        model.compile(loss=tf.keras.losses.Huber(), optimizer=tf.keras.optimizers.Adam(learning_rate=learning_rate),
                      metrics=['accuracy'])
        return model

    def train(self):
        if not self.is_training:
            return

        learning_rate = 0.7 # alpha
        discount_factor = 0.618 # gamma: how much future rewards should be decreased

        MIN_REPLAY_SIZE = 1000
        if len(self.replay_memory) < MIN_REPLAY_SIZE:
            return

        batch_size = 64 * 2
        batch = random.sample(self.replay_memory, batch_size)
        current_states = np.array([transition[0] for transition in batch])
        current_qs_list = self.model.predict(current_states, verbose=0)
        new_current_states = np.array([transition[3] for transition in batch])
        future_qs_list = self.target_model.predict(new_current_states, verbose=0)

        X = []
        Y = []
        for index, (observation, action, reward, new_observation, done) in enumerate(batch):
            if not done:
                max_future_q = reward + discount_factor * np.max(future_qs_list[index])
            else:
                max_future_q = reward

            current_qs = current_qs_list[index]
            current_qs[self.player.pieces.index(action)] = (1 - learning_rate) * current_qs[self.player.pieces.index(action)] + learning_rate * max_future_q

            X.append(observation)
            Y.append(current_qs)
        self.model.fit(np.array(X), np.array(Y), batch_size=batch_size, verbose=0, shuffle=True)

    def perform_action(self, pieces: list[Piece], rolled_number: int) -> None:
        self.steps_taken += 1

        current_state = self.player.get_player_state()

        # Explore using the Epsilon Greedy Exploration Strategy
        if self.is_training and np.random.rand() <= self.epsilon:
            # Explore
            choosen_piece_to_move = random.choice(pieces)
            is_exploring = True
        else:
            # Exploit
            predicted_qvalues = self.model.predict(current_state.reshape([1, current_state.shape[0]]), verbose=0)
            choosen_piece_to_move = pieces[np.argmax(predicted_qvalues)]
            is_exploring = False

        reward = self.game.execute_turn(choosen_piece_to_move)
        new_state = self.player.get_player_state()
        is_done = self.game.get_winner() is not None

        if self.last_current_state is not None:
            amount_of_kicked_pieces = sum(piece.got_kicked_last_round for piece in self.player.pieces)

            self.last_reward -= amount_of_kicked_pieces * 0.5 # TODO save the reward values in a new file as a constant

            if not self.last_is_exploring:
                self.total_training_rewards += self.last_reward
            
            self.replay_memory.append([self.last_current_state, self.last_moved_piece, self.last_reward, self.last_new_state, is_done])

        if is_done:
            self.replay_memory.append([current_state, choosen_piece_to_move, reward, new_state, is_done])
            if not self.last_is_exploring:
                self.total_training_rewards += reward
        else:
            self.last_reward = reward
            self.last_moved_piece = choosen_piece_to_move
            self.last_current_state = current_state
            self.last_new_state = new_state
            self.last_is_exploring = is_exploring

        if self.steps_taken % 4 == 0 or is_done:
            self.train()


        # why only when done?
        if is_done: # and self.steps_taken >= 100:
            self.target_model.set_weights(self.model.get_weights())

        self.calculate_epsilon()

    def calculate_epsilon(self):
        self.epsilon = self.min_epsilon + (self.max_epsilon - self.min_epsilon) * np.exp(-self.decay * self.episode)

    def reset(self):
        self.steps_taken = 0
        self.total_training_rewards = 0

    def save_weights(self, target_directory = "."):
        model_path = Path(target_directory).joinpath(model_path_string)
        target_model_path = Path(target_directory).joinpath(target_model_path_string)

        model_path.parent.mkdir(exist_ok=True)
        target_model_path.parent.mkdir(exist_ok=True)

        self.model.save(model_path)
        self.target_model.save(target_model_path)

    def load_weights(self, target_directory = "."):
        model_path = Path(target_directory).joinpath(model_path_string)
        target_model_path = Path(target_directory).joinpath(target_model_path_string)

        if model_path.exists and target_model_path.exists:
            self.model = tf.keras.models.load_model(model_path)
            self.target_model = tf.keras.models.load_model(target_model_path)
        else:
            print("Couldn't find model weights.")
            exit(1)
